'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = auth;

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _passportLocal = require('passport-local');

var _User = require('./models/User');

var _User2 = _interopRequireDefault(_User);

var _config = require('./config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// TODO: use jwt
// https://vladimirponomarev.com/blog/authentication-in-react-apps-jwt

function createToken(user) {
    var payload = {
        sub: user._id
    };

    return _jsonwebtoken2.default.sign(payload, _config.jwtSecret);
}

function auth() {
    _passport2.default.use('local-signup', new _passportLocal.Strategy({
        usernameField: 'email',
        passwordField: 'password',
        session: false,
        passReqToCallback: true
    }, function (req, email, password, done) {
        var user = new _User2.default({
            email: email.trim().toLowerCase(),
            password: password.trim(),
            name: req.body.name.trim()
        });

        user.save(function (error) {
            if (error) {
                done({ message: 'INCORRECT_EMAIL' });
            } else {
                var token = createToken(user);

                done(null, user, token);
            }
        });
    }));

    _passport2.default.use('local-login', new _passportLocal.Strategy({
        usernameField: 'email',
        passwordField: 'password',
        session: false,
        passReqToCallback: true
    }, function (req, email, password, done) {
        email = email.trim().toLowerCase();

        _User2.default.findOne({ email: email }, function (error, user) {
            if (error) {
                return done(error);
            }
            if (!user) {
                return done({ message: 'INCORRECT_EMAIL' });
            }

            return user.comparePassword(password, function (err, isMatch) {
                if (err) {
                    return done(err);
                }

                if (!isMatch) {
                    return done({ message: 'INCORRECT_PASSWORD' });
                }

                var token = createToken(user);

                done(null, user, token);
            });
        });
    }));
}