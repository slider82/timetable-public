import React, { Component } from 'react';
import _ from 'lodash';

const Validate = {
    name(name) {
        return name.trim();
    },

    students(students) {
        return students.length;
    },

    startEdu(start) {
        return /\d{1,2}\.\d{1,2}/.test(start);
    },

    endEdu(end) {
        return /\d{1,2}\.\d{1,2}/.test(end);
    },

    timeEdu(time) {
        return /\d{1,2}:\d{1,2}/.test(time);
    },

    daysEdu(days) {
        let valid = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];

        return days.some(day => valid.indexOf(day) !== -1);
    },

    creditsEdu(credits) {
        return typeof credits === 'number' && credits > 0 && credits <= 5;
    }
};


class GroupCreateForm extends Component {

    constructor(props) {
        super(props);

        const studyPeriod = this.getStudyPeriod();

        this.defaultValues = {
            name: '',
            students: '',
            lang: 'en',
            eduStart: studyPeriod.start,
            eduEnd: studyPeriod.end,
            eduTime: '9:00',
            eduCredits: 5
        };

        this.errorsDefault = {
            name: false,
            students: false,

            startEdu: false,
            endEdu: false,
            timeEdu: false,
            daysEdu: false,
            creditsEdu: false
        };

        this.state = {
            submitDisabled: true,
            error: false,
            errors: {
                ...this.errorsDefault
            }
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    validate(data) {
        let isValid;
        let errors = {};
        let startEduMonth = +data.edu.start.split('.')[1];
        let endEduMonth = +data.edu.end.split('.')[1];

        Object.keys(data).forEach(key => {
            if (key === 'edu') {
                Object.keys(data[key]).forEach(edu => {
                    let eduKey = edu + 'Edu';
                    let validateFunc = Validate[eduKey];

                    if (validateFunc) {
                        if (!validateFunc(data[key][edu])) {
                            errors[eduKey] = true;
                        }
                    }
                });
            } else {
                let validateFunc = Validate[key];

                if (validateFunc) {
                    if (!validateFunc(data[key])) {
                        errors[key] = true;
                    }
                }
            }
        });

        // если начальный месяц больше следующего то ошибка
        if (startEduMonth > endEduMonth) {
            errors.startEdu = true;
            errors.endEdu = true;
        }

        isValid = !Object.keys(errors).length;

        return {
            isValid,
            errors
        };
    }

    getStudyPeriod() {
        let period,
            periods = {
                autumn: {
                    start: '1.9',
                    end: '31.12'
                },
                spring: {
                    start: '1.2',
                    end: '31.5'
                }
            },
            // now = new Date(2016, 0, 1),
            now = new Date(),
            nowMonth = now.getMonth() + 1;

        if (nowMonth < 6) {
            period = periods.spring;
        } else {
            period = periods.autumn;
        }

        return period;
    }

    getValue(name) {
        return this.refs[name].value.trim();
    }

    setValue(name, value) {
        this.refs[name].value = value;
    }

    resetForm() {
        _.forOwn(this.defaultValues, (value, key) => this.setValue(key, value));
        const checkboxes = this.refs.form.querySelectorAll('input[type="checkbox"]');

        for (let input of checkboxes) {
            input.checked = false;
        }
    }

    getDays() {
        let days = this.refs.form.querySelectorAll('input[type="checkbox"]:checked'),
            result = [];

        for(let input of days) {
            result.push(input.name.split('.')[2])
        }

        return result;
    }

    getStudents() {
        return this.getValue('students')
                .split('\n')
                .map(item => item.trim()) // if do valid remove it
                .filter(item => item.length) // if do valid remove it
                .map(
                    item => item
                        .replace(/[^A-Za-zа-яА-я\s]/g, '')
                        .trim()
                        .replace(/\s+/g, ' ')
                        .split(' ')
                        .map(item => item && item[0].toUpperCase() + item.slice(1).toLowerCase())
                        .join(' ')
                )
                .filter(item => item.length)
                .sort();
    }

    onChange(event) {
        if (this.state.error) {
            this.setState({
                error: false,
                errors: {
                    ...this.errorsDefault
                }
            });
        }
    }

    onSubmit(event) {
        event.nativeEvent.preventDefault();

        let data = {
            teacher: this.getValue('teacher'),
            type: this.getValue('type'),
            name: this.getValue('name'),
            lang: this.getValue('lang'),
            students: this.getStudents(),
            edu: {
                start: this.getValue('eduStart'),
                end: this.getValue('eduEnd'),
                time: this.getValue('eduTime'),
                days: this.getDays(),
                credits: +this.getValue('eduCredits'),
            }
        };
        let validate = this.validate(data);

        if (validate.isValid) {
            this.props.onSubmit(data);
            this.onClose();
        } else {
            this.setState({
                error: true,
                errors: {
                    ...this.state.errors,
                    ...validate.errors
                }
            });
        }
    }

    onClose() {
        this.resetForm();
        this.props.onClose();
    }

    render() {
        let user = this.props.user,
            errors = this.state.errors;

        return(
            <div className={'modal fade' + (this.props.show ? ' in': '')}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <span className="close" onClick={this.onClose}>&times;</span>
                            <h4 className="modal-title">Создание группы</h4>
                        </div>
                        <form onSubmit={this.onSubmit} ref="form" className="form-horizontal">
                            <div className="modal-body">
                                <input
                                    type="hidden"
                                    name="teacher"
                                    ref="teacher"
                                    value={user._id} />
                                <input
                                    type="hidden"
                                    name="type"
                                    ref="type"
                                    defaultValue="main" />

                                <div className={'form-group' + (errors.name ? ' has-error' : '')}>
                                    <label className="col-sm-4 control-label">Название</label>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            name="name"
                                            ref="name"
                                            placeholder="Введите название группы"
                                            onChange={this.onChange}
                                            defaultValue={this.defaultValues.name} />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="col-sm-4 control-label">Язык</label>
                                    <div className="col-sm-8">
                                        <select
                                            className="form-control"
                                            name="lang"
                                            onChange={this.onChange}
                                            defaultValue="en"
                                            ref="lang">
                                                <option value="en">Английский</option>
                                                <option value="fr">Французский</option>
                                                <option value="de">Немецкий</option>
                                        </select>
                                    </div>
                                </div>
                                <div className={'form-group' + (errors.students ? ' has-error' : '')}>
                                    <label className="col-sm-4 control-label">Студенты</label>
                                    <div className="col-sm-8">
                                        <textarea
                                            className="form-control"
                                            name="students"
                                            placeholder="Введите список студентов, каждый с новой строки"
                                            rows="3"
                                            onChange={this.onChange}
                                            ref="students"
                                            defaultValue={this.defaultValues.students}
                                            ></textarea>
                                    </div>
                                </div>
                                <div className={'form-group' + (errors.startEdu ? ' has-error' : '')}>
                                    <label className="col-sm-4 control-label">Начало занятий</label>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            name="edu.start"
                                            ref="eduStart"
                                            placeholder="Введите дату начала занятий"
                                            onChange={this.onChange}
                                            defaultValue={this.defaultValues.eduStart} />
                                    </div>
                                </div>
                                <div className={'form-group' + (errors.endEdu ? ' has-error' : '')}>
                                    <label className="col-sm-4 control-label">Конец занятий</label>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            name="edu.end"
                                            ref="eduEnd"
                                            placeholder="Введите дату окончания занятий"
                                            onChange={this.onChange}
                                            defaultValue={this.defaultValues.eduEnd} />
                                    </div>
                                </div>
                                <div className={'form-group' + (errors.timeEdu ? ' has-error' : '')}>
                                    <label className="col-sm-4 control-label">Время занятий</label>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            name="edu.time"
                                            ref="eduTime"
                                            placeholder="Введите дату время занятия"
                                            onChange={this.onChange}
                                            defaultValue={this.defaultValues.eduTime} />
                                    </div>
                                </div>
                                <div className={'form-group' + (errors.daysEdu ? ' has-error' : '')}>
                                    <label className="col-sm-4 control-label">Дни недели: </label>
                                    <div className="col-sm-8">
                                        <label className="checkbox-inline">
                                            <input
                                                type="checkbox"
                                                name="edu.days.Mo"
                                                onChange={this.onChange}
                                                ref="eduDaysMo"/> Пн
                                        </label>

                                        <label className="checkbox-inline">
                                            <input
                                                type="checkbox"
                                                name="edu.days.Tu"
                                                onChange={this.onChange}
                                                ref="eduDaysTu" /> Вт
                                        </label>
                                        <label className="checkbox-inline">
                                            <input
                                                type="checkbox"
                                                name="edu.days.We"
                                                onChange={this.onChange}
                                                ref="eduDaysWe" /> Ср
                                        </label>
                                        <label className="checkbox-inline">
                                            <input
                                                type="checkbox"
                                                name="edu.days.Th"
                                                onChange={this.onChange}
                                                ref="eduDaysTh" /> Чт
                                        </label>
                                        <label className="checkbox-inline">
                                            <input
                                                type="checkbox"
                                                name="edu.days.Fr"
                                                onChange={this.onChange}
                                                ref="eduDaysFr" /> Пт
                                        </label>
                                        <label className="checkbox-inline">
                                            <input
                                                type="checkbox"
                                                name="edu.days.Sa"
                                                onChange={this.onChange}
                                                ref="eduDaysSa" /> Сб
                                        </label>
                                    </div>
                                </div>
                                <div className={'form-group' + (errors.creditsEdu ? ' has-error' : '')}>
                                    <label className="col-sm-4 control-label">Зачетных занятий</label>
                                    <div className="col-sm-8">
                                        <input
                                            className="form-control"
                                            type="text"
                                            name="edu.credits"
                                            ref="eduCredits"
                                            placeholder="Введите количество зачетных занятий"
                                            onChange={this.onChange}
                                            defaultValue={this.defaultValues.eduCredits} />
                                    </div>
                                </div>

                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" onClick={this.onClose}>Закрыть</button>
                                <input type="submit" value="Создать" className="btn btn-primary"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default GroupCreateForm;