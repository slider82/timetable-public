'use strict';

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _cookieParser = require('cookie-parser');

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _config = require('./config');

var _db = require('./db');

var _db2 = _interopRequireDefault(_db);

var _auth = require('./auth');

var _auth2 = _interopRequireDefault(_auth);

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isProduction = process.env.NODE_ENV === 'production';

var PORT = process.env.PORT || _config.port;
var MONGOOSE_URI = isProduction ? _config.mongooseUriProd : _config.mongooseUri;

// express
var app = (0, _express2.default)();

if (!isProduction) {
    // webpack
    var webpack = require('webpack');
    var webpackConfig = require('../webpack.config');
    var compiler = webpack(webpackConfig);

    app.use(require('webpack-dev-middleware')(compiler, {
        publicPath: webpackConfig.output.publicPath
    }));
    app.use(require('webpack-hot-middleware')(compiler));
}

app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: true }));
app.use((0, _cookieParser2.default)());

app.use(_passport2.default.initialize());
(0, _auth2.default)();

app.use('/auth', _routes2.default.auth);
app.use('/api', _routes2.default.api);

app.use('/static', _express2.default.static(_path2.default.resolve(__dirname, '../dist')));
app.get('*', function (req, res) {
    res.sendFile(_path2.default.join(__dirname, '../index.html'));
});

app.listen(PORT, function (err) {
    if (err) {
        return console.error(err);
    }

    console.log('Listening at http://localhost:' + PORT + '/');
});

(0, _db2.default)(MONGOOSE_URI);