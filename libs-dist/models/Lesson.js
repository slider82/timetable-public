'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LessonSchema = new _mongoose.Schema({
    groupId: {
        type: _mongoose.Schema.Types.ObjectId,
        ref: 'Group',
        require: true
    },
    date: {
        type: String
    },
    testPeriodNumber: {
        type: Number
    },
    student: {
        type: String,
        require: true
    },
    presence: {
        type: Boolean
    },
    evaluation: {
        type: Number
    },
    credit: {
        type: Boolean
    }
});

exports.default = _mongoose2.default.model('Lesson', LessonSchema);