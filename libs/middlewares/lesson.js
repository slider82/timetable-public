import _ from 'lodash';
import Lesson from '../models/Lesson';

function validate(data) {
    let valid = {
        isValid: true
    };

    if (!data.groupId) {
        valid = {
            isValid: false,
            error: {
                message: 'Cant find groupId'
            }
        }
    } else if (!data.date && !data.testPeriodNumber) {
        valid = {
            isValid: false,
            error: {
                message: 'Add date or testPeriodNumber'
            }
        }
    }

    return valid;
}


export function getLessons(req, res) {
    const body = req.body;

    if (body.groupId) {
        Lesson.find({
            groupId: body.groupId
        }, (error, lessons) => {
            if (error) {
                res.json({
                    error
                });
            } else {
                let result = {};

                lessons.forEach(lesson => {
                    _.merge(result, {
                        [lesson.groupId]: {
                            [lesson.student]: {
                                [lesson.date]: lesson
                            }
                        }
                    });
                });

                res.json(result);
            }
        });
    } else {
        res.json({
            error: {
                message: 'Not groupId'
            }
        });
    }
}

export function saveLesson(req, res) {
    const body = req.body;
    const params = {
        groupId: body.groupId,
        student: body.student,
        date: body.date ? body.date : null,
        testPeriodNumber: body.testPeriodNumber ? body.testPeriodNumber : null,
        presence: body.presence !== null ? body.presence : null,
        evaluation: body.evaluation ? body.evaluation : null,
        credit: body.credit !== null ? body.credit : null
    };

    if (body._id) {
        params.id = body._id;
    }

    const valid = validate(params);

    // TODO: реализовать валидацию
    if (valid.isValid) {
        // update
        if (params.id) {
            Lesson.findById(params.id, (error, lesson) => {
                if (error) {
                    console.log(error);
                    res.json({
                        error
                    });
                } else {
                    if (params.credit) {
                        lesson.evaluation = null;
                        lesson.presence = null;
                        lesson.credit = params.credit;
                    } else if (params.evaluation) {
                        lesson.evaluation = params.evaluation;
                        lesson.presence = null;
                        lesson.credit = null;
                    } else {
                        lesson.presence = params.presence;
                        lesson.evaluation = null;
                        lesson.credit = null;
                    }

                    lesson.save((error, lesson) => {
                        if (error) {
                            // TODO: обработать ошибки в клиенте
                            console.log(error);
                            res.json({
                                error
                            });
                        } else {

                            res.json(lesson.toJSON());
                        }
                    });
                }
            });
        // create
        } else {
            const lesson = new Lesson(params);

            lesson.save((error, lesson) => {
                if (error) {
                    // TODO: обработать ошибки в клиенте
                    console.log(error);
                    res.json({
                        error
                    });
                } else {
                    res.json(lesson.toJSON());
                }
            });
        }

    } else {
        // TODO: обработать ошибки в клиенте
        res.json({
            error: valid.error
        });
    }
}