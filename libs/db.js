import mongoose from 'mongoose';
import Promise from 'bluebird';

mongoose.Promise = Promise;

export default function db (url) {
    mongoose.connect(url);

    const db = mongoose.connection;

    db.on('error', err => {
        console.log('connection error:', err.message);
    });

    db.on('open', err => {
        console.log('Connected DB:');
    });
}