import { createStore, applyMiddleware } from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';
import _ from 'lodash';

import rootReducer from '../reducers';

export default function configureStore() {
    const logger = createLogger();
    const initialState = {};

    const user = localStorage.getItem('user');

    if (user) {
        initialState.user = JSON.parse(user);
    }

    const store = createStore(
        rootReducer,
        initialState,
        applyMiddleware(
            thunk,
            logger,
            routerMiddleware(browserHistory)
        )
    );

    let currentUser = store.getState().user;
    let unsubscribe = store.subscribe(() => {
        let previousUser = currentUser;

        currentUser = _.pick(store.getState().user, ['name', 'email', 'token', 'isAuthorized', '_id']);

        if (!previousUser.token && currentUser.token && previousUser.token !== currentUser.token) {
            localStorage.setItem('user', JSON.stringify(currentUser));
        } else if (previousUser.token && !currentUser.token) {
            localStorage.removeItem('user');
        }
    });

    // if (module.hot) {
    //     module.hot.accept('../reducers', () => {
    //         const nextRootReducer = require('../reducers');
    //         store.replaceReducer(nextRootReducer);
    //     })
    // }

    return store;
}