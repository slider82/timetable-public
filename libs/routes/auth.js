import { Router } from 'express';
import passport from 'passport';

const router = Router();

router.post('/login', (req, res, next) => {
    if (!req.body) {
        res
            .status(401)
            .json({
                error: {
                    message: 'BAD_REQUEST'
                }
            });
    } else {
        const email = req.body.email.trim();
        const password = req.body.password.trim();

        if (!email || !password) {
            let error;

            if (!email && !password) {
                error = 'EMPTY_FIELDS';
            } else if (!email) {
                error = 'INCORRECT_EMAIL';
            } else if (!password) {
                error = 'INCORRECT_PASSWORD';
            }

            res
                .status(401)
                .json({
                    error: {
                        message: error
                    }
                });
        } else {
            passport.authenticate('local-login', (error, user, token) => {
                if (error) {
                    // TODO: нужно унифицировать и обработать в клиенте
                    res
                        .status(401)
                        .json({
                            error
                        });
                } else {
                    res.json({
                        user,
                        token
                    });
                }
            })(req, res, next);
        }
    }
});

router.post('/signup', (req, res, next) => {
    if (!req.body) {
        res
            .status(401)
            .json({
                error: {
                    message: 'BAD_REQUEST'
                }
            });
    } else {
        const email = req.body.email.trim();
        const password = req.body.password.trim();
        const name = req.body.name.trim();

        if (!email || !password || !name) {
            let error;

            if (!email && !password && !name) {
                error = 'EMPTY_FIELDS';
            } else if (!email) {
                error = 'INCORRECT_EMAIL';
            } else if (!password) {
                error = 'INCORRECT_PASSWORD';
            } else if (!name) {
                error = 'INCORRECT_NAME';
            }

            res
                .status(401)
                .json({
                    error: {
                        message: error
                    }
                });
        } else {
            passport.authenticate('local-signup', (error, user, token) => {
                if (error) {
                    res
                        .status(401)
                        .json({
                            error
                        });
                } else {
                    res.json({
                        user,
                        token
                    });
                }
            })(req, res, next);
        }
    }
});

export default router;