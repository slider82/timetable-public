import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { push } from 'react-router-redux';

import { login } from '../actions/LoginActions';

class Login extends Component {
    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.onInputFocus = this.onInputFocus.bind(this);
        this.onInputChange = this.onInputChange.bind(this);

        this.state = {
            error: false,
            errorEmail: false,
            errorPassword: false,
            submitDisabled: true
        };
    }

    componentWillReceiveProps(nextProps) {
        const { user, dispatch, location } = nextProps;

        if (user.error) {
            let error  = {
                error: true
            };

            switch (user.error.message) {
                case 'INCORRECT_EMAIL':
                    error.errorEmail = true;
                    break;
                case 'INCORRECT_PASSWORD':
                    error.errorPassword = true;
                    break;
                case 'EMPTY_FIELDS':
                    error.errorEmail = true;
                    error.errorPassword = true;
                    break;
            }

            this.setState(error);
        }

        if (user.isAuthorized) {
            let next = location.query.next || '/';

            dispatch(push(next));
        }
    }

    onInputFocus() {
        if (this.state.error) {
            let error = {
                error: false
            };

            if (this.state.errorEmail) {
                error.errorEmail = false;
                this.emailInput.value = '';
            }

            if (this.state.errorPassword) {
                error.errorPassword = false;
                this.passwordInput.value = '';
            }

            this.setState(error);
        }
    }

    onInputChange() {
        const formData = this.getFormFields();
        let submitDisabled;

        submitDisabled = !Object.keys(formData).every(key => formData[key]);

        if (this.state.submitDisabled !== submitDisabled) {
            this.setState({
                submitDisabled
            });
        }
    }

    onSubmit(event) {
        event.nativeEvent.preventDefault();

        if (this.validate()) {
            let data = this.getFormFields();

            this.props.login(data);

        } else {

        }
    }

    getFormFields() {
        return {
            email: this.emailInput.value.trim(),
            password: this.passwordInput.value.trim()
        }
    }

    validate() {
       return true;
    }

    render() {
        const { user } = this.props;

        return(
            <div className="container">
                <div className="col-sm-6 col-sm-offset-3">
                    <form onSubmit={this.onSubmit} className="form-horizontal">
                        <div className="col-xs-offset-2 col-xs-10">
                            <h1>Авторизация</h1>
                        </div>

                        {this.state.error ?
                            <div className="col-xs-offset-2 col-xs-10">
                                <div className="alert alert-danger" role="alert">Вы ввели не верные данные</div>
                            </div>
                        : null}

                        <div className={'form-group' + (this.state.errorEmail ? ' has-error' : '')}>
                            <label htmlFor="" className="control-label col-xs-2">Email</label>
                            <div className="col-xs-10">
                                <input
                                    className="form-control"
                                    type="text"
                                    name="email"
                                    placeholder="Введите email"
                                    ref={email => this.emailInput = email}
                                    onChange={this.onInputChange}
                                    onFocus={this.onInputFocus}
                                />
                            </div>
                        </div>
                        <div className={'form-group' + (this.state.errorPassword ? ' has-error' : '')}>
                            <label htmlFor="" className="control-label col-xs-2">Пароль</label>
                            <div className="col-xs-10">
                                <input
                                    className="form-control"
                                    type="password"
                                    name="password"
                                    placeholder="Введите пароль"
                                    ref={password => this.passwordInput = password}
                                    onChange={this.onInputChange}
                                    onFocus={this.onInputFocus}
                                />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-xs-offset-2 col-xs-10">
                                <button className="btn btn-primary" disabled={this.state.submitDisabled} style={{
                                    marginRight: '10px'
                                }}>Авторизоваться</button>
                                <Link to="/signup" className="btn btn-default">Регистрация</Link>
                                {user.fetching ? 'Авторизация...' : ''}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        login: bindActionCreators(login, dispatch),
        dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);