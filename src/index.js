import React, { Component } from 'react';
import ReactDOM from 'react-dom';
// import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import Routes from './Routes';
import configureStore from './store/configureStore';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
	<Provider store={store}>
		{/*<AppContainer>*/}
			<Routes history={history} />
		{/*</AppContainer>*/}
	</Provider>,
	document.getElementById('root')
);

// if (!isProduction) {
//     if (module.hot) {
//         module.hot.accept('./App', () => {
//             const NextRouters= require('./Routes').default;
//
//             ReactDOM.render(
// 				<Provider store={store}>
// 					<AppContainer>
// 						<NextRouters history={history} />
// 					</AppContainer>
// 				</Provider>,
//                 document.getElementById('root')
//             );
//         });
//     }
// }
// https://github.com/jpsierens/webpack-react-redux/blob/master/app/index.js