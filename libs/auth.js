import jwt from 'jsonwebtoken';
import passport from 'passport';
import { Strategy } from 'passport-local';
import User from './models/User';
import { jwtSecret } from './config';

// TODO: use jwt
// https://vladimirponomarev.com/blog/authentication-in-react-apps-jwt

function createToken(user) {
    const payload = {
        sub: user._id
    };

    return jwt.sign(payload, jwtSecret)
}

export default function auth() {
    passport.use('local-signup', new Strategy({
        usernameField: 'email',
        passwordField: 'password',
        session: false,
        passReqToCallback: true
    }, (req, email, password, done) => {
        const user = new User({
            email: email.trim().toLowerCase(),
            password: password.trim(),
            name: req.body.name.trim()
        });

        user.save((error) => {
            if (error) {
                done({ message: 'INCORRECT_EMAIL' });
            } else {
                const token = createToken(user);

                done(null, user, token);
            }
        });
    }));


    passport.use('local-login', new Strategy({
        usernameField: 'email',
        passwordField: 'password',
        session: false,
        passReqToCallback: true
    }, (req, email, password, done) => {
        email = email.trim().toLowerCase();

        User.findOne({ email }, (error, user) => {
            if (error) {
                return done(error);
            }
            if (!user) {
                return done({ message: 'INCORRECT_EMAIL' });
            }

            return user.comparePassword(password, (err, isMatch) => {
                if (err) {
                    return done(err);
                }

                if (!isMatch) {
                    return done({ message: 'INCORRECT_PASSWORD' });
                }

                const token = createToken(user);

                done(null, user, token)
            });
        });
    }));

}