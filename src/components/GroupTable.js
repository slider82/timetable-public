import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import { LESSON_DEFAULT_PARAMS } from '../constaint/lessons';
import {
    saveLesson,
    getLesson
} from '../actions/LessonActions';

import GroupRow from './GroupRow';
import LessonForm from './LessonForm';

class GroupTable extends Component {

    constructor(props) {
        super(props);

        this.state = {
            lesson: {
                show: false
            },
            currentDate: this.props.group.edu.currentDate
        };

        this.onShowLessonForm = this.onShowLessonForm.bind(this);
        this.onHideLessonForm = this.onHideLessonForm.bind(this);
        this.onLessonSubmit = this.onLessonSubmit.bind(this);
        this.onDateClick = this.onDateClick.bind(this);
    }

    componentDidMount() {
        const { _id } = this.props.group;

        if (!this.props.lessons[_id]) {
            this.props.getLesson(_id);
        }

        if (this.container) {
            const studentWidth = this.studentTh.offsetWidth + 1 + 'px';

            this.studentTableFixed.style.width = studentWidth;
            this.studentTableFixed.querySelector('th').style.width = studentWidth;
            this.studentTableFixed.classList.add('show');

            this.currentMakeCenter();
        }
    }

    onShowLessonForm(lesson = {}) {
        this.setState({
            lesson: {
                ...lesson,
                show: true
            }
        });
    }

    onHideLessonForm() {
        this.setState({
            lesson: {
                show: false
            }
        });
    }

    onLessonSubmit(props) {
        const lessonProps = _.merge({}, _.pick(this.state.lesson, LESSON_DEFAULT_PARAMS), {
            student: this.state.lesson.student.id,
            ...props
        });

        this.props.saveLesson(lessonProps);

        this.setState({
            lesson: {
                show: false
            }
        });
    }

    onDateClick(event) {
        const target = event.target;

        this.setState({
            currentDate: target.innerText
        });
    }

    currentMakeCenter() {
        const studentWidth = this.studentTh.offsetWidth;
        const currentTh = this.container.querySelector('th.current');
        const visibleWidth = this.container.offsetWidth - studentWidth;
        const currentLeft = currentTh.getBoundingClientRect().left - this.container.getBoundingClientRect().left - studentWidth;

        this.container.scrollLeft = currentLeft - visibleWidth / 2 + currentTh.offsetWidth / 2;
    }

    render() {
        const group = this.props.group;
        const { _id, lang } = group;
        const groupLessons = this.props.lessons[_id] || {};
        const currentDate = this.state.currentDate;
        let dates = group.edu.dates;
        let students = group.students.map(student => {
            return _.merge({}, student, {
                shortName: student.name
                    .split(' ')
                    .map((item, index) => index === 0 ? item : item[0].toUpperCase() + '.')
                    .join(' ')
            });
        });

        if (group.students.length) {
            return <div>
                <LessonForm {...this.state.lesson} onSubmit={this.onLessonSubmit} onCancel={this.onHideLessonForm} />
                <h1>{group.name}</h1>
                <table className="table student-table-fixed" ref={table => this.studentTableFixed = table}>
                    <thead>
                    <tr>
                        <th>Студент</th>
                    </tr>
                    </thead>
                    <tbody>
                    {students.map(student => {
                        return <tr key={student.id}>
                            <td>{student.shortName}</td>
                        </tr>;
                    })}
                    </tbody>
                </table>
                <div className="lesson-table-container" ref={container => this.container = container}>
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th className="th-student td-student" ref={th => this.studentTh = th}>Студент</th>
                                <th className="lang">Язык</th>
                                {dates.map(date => <th
                                    key={date}
                                    onClick={this.onDateClick}
                                    className={currentDate === date ? 'current' : ''}>
                                        {date}
                                </th>)}
                            </tr>
                        </thead>
                        <tbody>
                            {students.map(student => {
                                const lessons = groupLessons[student.id] || {};
                                const groupRowProps = {
                                    groupId: _id,
                                    lang,
                                    dates,
                                    student,
                                    lessons,
                                    currentDate,
                                    onShowLessonForm: this.onShowLessonForm
                                };

                                return <GroupRow
                                    key={student.id}
                                    {...groupRowProps}
                                />;
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        } else {
            return <div>
                <h1>{group.name}</h1>
                Нет студентов
            </div>;
        }
    }
}

function mapStateToProps(state) {
    return {
        lessons: state.lessons
    }
}
function mapDispatchTopProps(dispatch) {
    return {
        saveLesson: bindActionCreators(saveLesson, dispatch),
        getLesson: bindActionCreators(getLesson, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchTopProps)(GroupTable);