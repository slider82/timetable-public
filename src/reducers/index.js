import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'

import user from './user';
import groups from './groups';
import lessons from './lessons';

export default combineReducers({
    user,
    groups,
    lessons,
    routing: routerReducer
})