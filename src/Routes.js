import React, { Component} from 'react';
import { Router, Route } from 'react-router';

import App from './App';
import Main from './components/Main';
import Login from './containers/Login';
import Signup from './containers/Signup';
import Groups from './containers/Groups';
import Group from './containers/Group';
import { requireAuthentication } from './components/Authenticated';

export default class Routers extends  Component {
    render() {
        return <Router history={this.props.history}>
            <Route path="/" component={requireAuthentication(Main)}/>
            <Route path="/" component={App}>
                <Route path="groups" component={requireAuthentication(Groups)}/>
                <Route path="group/:id" component={requireAuthentication(Group)}/>
                <Route path="login" component={Login}/>
                <Route path="signup" component={Signup}/>
                {/*<Route path="*" component={NotFound} />*/}
            </Route>
        </Router>
    }
}