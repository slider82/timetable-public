import fetch from '../api/fetch';
import {
    GROUPS_GET_FETCHING,
    GROUPS_GET_SUCCESS,
    GROUP_CREATE_REQUEST,
    GROUP_CREATE_SUCCESS
} from '../constaint/groups';

export function getGroups(options = {}) {
    return function (dispatch) {
        dispatch({
            type: GROUPS_GET_FETCHING
        });

        fetch('/api/group/get')
            .then(response => {
                const { groups } = response;

                dispatch({
                    type: GROUPS_GET_SUCCESS,
                    groups
                });
            });
    }
}

export function createGroup(data = {}) {
    return function (dispatch) {
        dispatch({
            type: GROUP_CREATE_REQUEST
        });
        fetch('/api/group/create', {
            method: 'POST',
            body: JSON.stringify(data)
        }).then(group => {
            dispatch({
                type: GROUP_CREATE_SUCCESS,
                group: group
            });
        }).catch(err => {
            console.log(err)
        });
    }
}