import React, { Component } from 'react';
import _ from 'lodash';

import { LESSON_DEFAULT_PARAMS } from '../constaint/lessons';

class StudentDateItem extends Component {
    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
    }
    onClick(event) {
        this.props.onShowLessonForm(_.pick(this.props, LESSON_DEFAULT_PARAMS));
    }
    render() {
        const { currentDate, date } = this.props;
        let presence, evaluation, credit;

        if (this.props._id) {
            if (this.props.credit) {
                // credit = 'З.';
                credit = <span className="glyphicon glyphicon-ok"></span>;
            } else {
                if (this.props.evaluation) {
                    evaluation = this.props.evaluation;
                } else {
                    // presence = this.props.presence ? 'Б.' : 'H/Б';
                    presence = this.props.presence ? <span className="glyphicon glyphicon-plus"></span> :
                        <span className="glyphicon glyphicon-minus"></span>;
                }
            }

        }

        return(
            <td onClick={this.onClick} className={currentDate === date ? 'current' : ''}>
                {presence}
                {evaluation}
                {credit}
            </td>
        );
    }
}

export default StudentDateItem;