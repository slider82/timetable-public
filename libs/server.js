import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import passport from 'passport';

import {
    port,
    mongooseUri,
    mongooseUriProd
} from'./config';
import db from './db';
import auth from './auth';
import routes from './routes';

const isProduction = process.env.NODE_ENV === 'production';

const PORT = process.env.PORT || port;
const MONGOOSE_URI = isProduction ? mongooseUriProd : mongooseUri;

// express
const app = express();

if (!isProduction) {
    // webpack
    const webpack = require('webpack');
    const webpackConfig = require('../webpack.config');
    const compiler = webpack(webpackConfig);

    app.use(require('webpack-dev-middleware')(compiler, {
        publicPath: webpackConfig.output.publicPath
    }));
    app.use(require('webpack-hot-middleware')(compiler));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(passport.initialize());
auth();

app.use('/auth', routes.auth);
app.use('/api', routes.api);

app.use('/static', express.static(path.resolve(__dirname, '../dist')));
app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, '../index.html'));
});

app.listen(PORT, function (err) {
    if (err) {
        return console.error(err);
    }

    console.log(`Listening at http://localhost:${PORT}/`);
});

db(MONGOOSE_URI);
