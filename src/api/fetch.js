import 'whatwg-fetch';
import _ from 'lodash';

export default function (url, options = {
    method: 'GET',
}) {
    let headers = {
        'Content-Type': 'application/json'
    };
    let user = localStorage.getItem('user');

    if (user) {
        user = JSON.parse(user);

        if (user.token) {
            headers.Authorization = 'bearer ' + user.token;
        }
    }

    return fetch(url, _.assign({
        headers,
        //credentials: 'same-origin'
    }, options)).then(res => {
        return res.json();
    });
}