'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserSchema = new _mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true,
        unique: true
    },
    password: {
        type: String,
        require: true
    }
});

UserSchema.methods.comparePassword = function (password, callback) {
    return _bcrypt2.default.compare(password, this.password, callback);
};

UserSchema.pre('save', function (next) {
    var user = this;

    if (!user.isModified('password')) return next();

    return _bcrypt2.default.genSalt(function (saltError, salt) {
        if (saltError) {
            return next(saltError);
        }

        return _bcrypt2.default.hash(user.password, salt, function (hashError, hash) {
            if (hashError) {
                return next(hashError);
            }

            user.password = hash;

            return next();
        });
    });
});

UserSchema.set('toJSON', {
    transform: function transform(doc, ret, options) {
        var retJson = {
            _id: ret._id,
            email: ret.email,
            name: ret.name
        };

        return retJson;
    }
});

exports.default = _mongoose2.default.model('User', UserSchema);