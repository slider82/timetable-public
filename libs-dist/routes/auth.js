'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = (0, _express.Router)();

router.post('/login', function (req, res, next) {
    if (!req.body) {
        res.status(401).json({
            error: {
                message: 'BAD_REQUEST'
            }
        });
    } else {
        var email = req.body.email.trim();
        var password = req.body.password.trim();

        if (!email || !password) {
            var error = void 0;

            if (!email && !password) {
                error = 'EMPTY_FIELDS';
            } else if (!email) {
                error = 'INCORRECT_EMAIL';
            } else if (!password) {
                error = 'INCORRECT_PASSWORD';
            }

            res.status(401).json({
                error: {
                    message: error
                }
            });
        } else {
            _passport2.default.authenticate('local-login', function (error, user, token) {
                if (error) {
                    // TODO: нужно унифицировать и обработать в клиенте
                    res.status(401).json({
                        error: error
                    });
                } else {
                    res.json({
                        user: user,
                        token: token
                    });
                }
            })(req, res, next);
        }
    }
});

router.post('/signup', function (req, res, next) {
    if (!req.body) {
        res.status(401).json({
            error: {
                message: 'BAD_REQUEST'
            }
        });
    } else {
        var email = req.body.email.trim();
        var password = req.body.password.trim();
        var name = req.body.name.trim();

        if (!email || !password || !name) {
            var error = void 0;

            if (!email && !password && !name) {
                error = 'EMPTY_FIELDS';
            } else if (!email) {
                error = 'INCORRECT_EMAIL';
            } else if (!password) {
                error = 'INCORRECT_PASSWORD';
            } else if (!name) {
                error = 'INCORRECT_NAME';
            }

            res.status(401).json({
                error: {
                    message: error
                }
            });
        } else {
            _passport2.default.authenticate('local-signup', function (error, user, token) {
                if (error) {
                    res.status(401).json({
                        error: error
                    });
                } else {
                    res.json({
                        user: user,
                        token: token
                    });
                }
            })(req, res, next);
        }
    }
});

exports.default = router;