'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var GroupSchema = new _mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    teacher: {
        type: _mongoose.Schema.Types.ObjectId,
        ref: 'User',
        require: true
    },
    type: {
        type: String,
        require: true,
        enum: ['main', 'sub']
    },
    lang: {
        type: String,
        require: true,
        enum: ['en', 'fr', 'de']
    },
    students: [{
        id: {
            type: Number
        },
        name: {
            type: String
        }
    }],
    subGroup: [{
        type: _mongoose.Schema.Types.ObjectId,
        ref: 'Group'
    }],

    edu: {
        start: {
            type: String
        },
        end: {
            type: String
        },
        days: [{
            type: String,
            enum: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
        }],
        time: {
            type: String
        },
        credits: {
            type: Number
        }
    }

});

exports.default = _mongoose2.default.model('Group', GroupSchema);