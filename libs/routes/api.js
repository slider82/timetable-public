import { Router } from 'express';
import jwt from 'jsonwebtoken';

import { jwtSecret } from '../config';
import User from '../models/User';

import { createGroup, getGroups } from '../middlewares/group';
import { saveLesson, getLessons } from '../middlewares/lesson';

const router = Router();

// check auth
router.use((req, res, next) => {
    const auth = req.headers.authorization;

    if (!auth) {
        res.status(401).json({
            error: 'No auth'
        });
    } else {
        const token = auth.split(' ')[1];

        if (token) {
            jwt.verify(token, jwtSecret, (err, decoded) => {
                if (err) {
                    res.status(401).json({
                        error: 'No auth'
                    });
                } else {
                    const userId = decoded.sub;

                    User.findById(userId, (err, user) => {
                        if (err) {
                            res.status(401).json({
                                error: 'User not found'
                            });
                        } else {
                            req.user = user;
                            next();
                        }
                    });
                }
            })
        }
    }
});

router.post('/group/create', createGroup);
router.get('/group/get', getGroups);

router.post('/lesson/save', saveLesson);
router.post('/lesson/get', getLessons);

export default router;