import fetch from '../api/fetch';
import {
    LESSON_SAVE_SEND,
    LESSON_SAVE_SUCCESS,
    LESSON_GET_SEND,
    LESSON_GET_SUCCESS
} from '../constaint/lessons';

export function saveLesson(lesson = {}) {
    return function (dispatch) {
          dispatch({
              type: LESSON_SAVE_SEND
          });

          fetch('/api/lesson/save', {
              method: 'POST',
              body: JSON.stringify(lesson)
          }).then(lesson => {
              dispatch({
                  type: LESSON_SAVE_SUCCESS,
                  lesson
              });
          });
    };
}

export function getLesson(groupId) {
    return function (dispatch) {

        dispatch({
            type: LESSON_GET_SEND
        });

        fetch('/api/lesson/get', {
            method: 'POST',
            body: JSON.stringify({
                groupId: groupId
            })
        }).then(lessons => {
            dispatch({
                type: LESSON_GET_SUCCESS,
                lessons,
                groupId
            });
        });
    }

}