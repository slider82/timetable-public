'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _merge3 = require('lodash/merge');

var _merge4 = _interopRequireDefault(_merge3);

exports.getLessons = getLessons;
exports.saveLesson = saveLesson;

var _Lesson = require('../models/Lesson');

var _Lesson2 = _interopRequireDefault(_Lesson);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function validate(data) {
    var valid = {
        isValid: true
    };

    if (!data.groupId) {
        valid = {
            isValid: false,
            error: {
                message: 'Cant find groupId'
            }
        };
    } else if (!data.date && !data.testPeriodNumber) {
        valid = {
            isValid: false,
            error: {
                message: 'Add date or testPeriodNumber'
            }
        };
    }

    return valid;
}

function getLessons(req, res) {
    var body = req.body;

    if (body.groupId) {
        _Lesson2.default.find({
            groupId: body.groupId
        }, function (error, lessons) {
            if (error) {
                res.json({
                    error: error
                });
            } else {
                var result = {};

                lessons.forEach(function (lesson) {
                    (0, _merge4.default)(result, _defineProperty({}, lesson.groupId, _defineProperty({}, lesson.student, _defineProperty({}, lesson.date, lesson))));
                });

                res.json(result);
            }
        });
    } else {
        res.json({
            error: {
                message: 'Not groupId'
            }
        });
    }
}

function saveLesson(req, res) {
    var body = req.body;
    var params = {
        groupId: body.groupId,
        student: body.student,
        date: body.date ? body.date : null,
        testPeriodNumber: body.testPeriodNumber ? body.testPeriodNumber : null,
        presence: body.presence !== null ? body.presence : null,
        evaluation: body.evaluation ? body.evaluation : null,
        credit: body.credit !== null ? body.credit : null
    };

    if (body._id) {
        params.id = body._id;
    }

    var valid = validate(params);

    // TODO: реализовать валидацию
    if (valid.isValid) {
        // update
        if (params.id) {
            _Lesson2.default.findById(params.id, function (error, lesson) {
                if (error) {
                    console.log(error);
                    res.json({
                        error: error
                    });
                } else {
                    if (params.credit) {
                        lesson.evaluation = null;
                        lesson.presence = null;
                        lesson.credit = params.credit;
                    } else if (params.evaluation) {
                        lesson.evaluation = params.evaluation;
                        lesson.presence = null;
                        lesson.credit = null;
                    } else {
                        lesson.presence = params.presence;
                        lesson.evaluation = null;
                        lesson.credit = null;
                    }

                    lesson.save(function (error, lesson) {
                        if (error) {
                            // TODO: обработать ошибки в клиенте
                            console.log(error);
                            res.json({
                                error: error
                            });
                        } else {

                            res.json(lesson.toJSON());
                        }
                    });
                }
            });
            // create
        } else {
            var lesson = new _Lesson2.default(params);

            lesson.save(function (error, lesson) {
                if (error) {
                    // TODO: обработать ошибки в клиенте
                    console.log(error);
                    res.json({
                        error: error
                    });
                } else {
                    res.json(lesson.toJSON());
                }
            });
        }
    } else {
        // TODO: обработать ошибки в клиенте
        res.json({
            error: valid.error
        });
    }
}