import mongoose, { Schema } from 'mongoose';

const LessonSchema = new Schema({
    groupId: {
        type: Schema.Types.ObjectId,
        ref: 'Group',
        require: true
    },
    date: {
        type: String
    },
    testPeriodNumber: {
        type: Number
    },
    student: {
        type: String,
        require: true
    },
    presence: {
        type: Boolean
    },
    evaluation: {
        type: Number
    },
    credit: {
        type: Boolean
    }
});

export default mongoose.model('Lesson', LessonSchema);