import React, { Component } from 'react'
import _ from 'lodash';

class LessonForm extends  Component {
    constructor(props) {
        super(props);

        this.state = {
            evaluation: '',
            presence: true,
            credit: false
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.show && nextProps.show) {
            this.setState({
                evaluation: nextProps.evaluation || '',
                presence: (nextProps.presence === false ? false : true),
                credit: !!nextProps.credit
            });
        }
    }

    onPresenceChange = (event) => {
        this.setState({
            presence: !!+event.target.value
        });
    };

    onEvaluationChange = (event) => {
        const evaluation = event.target.value;

        if (!_.isNaN(+evaluation)) {
            this.setState({
                evaluation
            });
        } else {
            this.setState({
                evaluation: ''
            });
        }

    };

    onCreditChange = (event) => {
        const checked = event.target.checked;

        this.setState({
            credit: checked
        });
    };

    onSubmit = (event) => {
        let props = {
            credit: null,
            presence: null,
            evaluation: null
        };

        event.preventDefault();

        if (this.state.credit) {
            props.credit = true;
        } else {
            if (this.state.evaluation) {
                props.evaluation = +this.state.evaluation;
            } else {
                props.presence = this.state.presence;
            }
        }

        this.reset();

        this.props.onSubmit(props);
    };

    onCancel = (event) => {
        event.preventDefault();
        this.reset();
        this.props.onCancel();
    };

    reset() {
        this.setState({
            evaluation: '',
            presence: true,
            credit: false
        });
    }

    render() {
        const { student, date} = this.props;

        let disableEvaluation = false,
            disablePresence = false,
            disableCredit = false;

        if (this.state.credit) {
            disableEvaluation = true;
            disablePresence = true;
            disableCredit = false;
        } else {
            if (this.state.evaluation) {
                disableEvaluation = false;
                disablePresence = true;
                disableCredit = true;
            } else {
                if (this.state.presence === false) {
                    disableEvaluation = true;
                    disablePresence = false;
                    disableCredit = true;
                } else {
                    disableEvaluation = false;
                    disablePresence = false;
                    disableCredit = false;
                }
            }
        }

        return(
            <div className={'modal fade' + (this.props.show ? ' in' : '')}>
                <div className="modal-dialog modal-sm">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">{date}: { student && student.shortName}</h4>
                        </div>
                        <form onSubmit={this.onSubmit} ref="form" className="form-horizontal">
                            <div className="modal-body">
                                <div className="form-group">
                                    <label className="col-xs-5 control-label">Оценка:</label>
                                    <div className="col-xs-7">
                                        <input
                                            className="form-control"
                                            type="text"
                                            placeholder="Оценка"
                                            onChange={this.onEvaluationChange}
                                            disabled={disableEvaluation}
                                            value={this.state.evaluation} />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="col-xs-5 control-label">Присутствие:</label>
                                    <div className="col-xs-7">
                                        <label className="radio-inline">
                                            <input
                                                type="radio"
                                                value="1"
                                                name="presence"
                                                onChange={this.onPresenceChange}
                                                disabled={disablePresence}
                                                checked={this.state.presence}/> Был
                                        </label>
                                        <label className="radio-inline">
                                            <input
                                                type="radio"
                                                name="presence"
                                                value="0"
                                                onChange={this.onPresenceChange}
                                                disabled={disablePresence}
                                                checked={!this.state.presence}/> Не был
                                        </label>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="col-xs-5 control-label">Зачет:</label>
                                    <div className="col-xs-7">
                                        <label className="checkbox-inline">
                                            <input
                                                type="checkbox"
                                                onChange={this.onCreditChange}
                                                disabled={disableCredit}
                                                checked={this.state.credit}/> &nbsp;
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <input
                                    className="btn btn-default"
                                    type="button"
                                    onClick={this.onCancel}
                                    value="Закрыть"/>
                                <input
                                    className="btn btn-primary"
                                    type="submit"
                                    value="Заполнить"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default LessonForm;