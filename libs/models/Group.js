import mongoose, { Schema } from 'mongoose';

const GroupSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    teacher: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        require: true
    },
    type: {
        type: String,
        require: true,
        enum: ['main', 'sub']
    },
    lang: {
        type: String,
        require: true,
        enum: ['en', 'fr', 'de']
    },
    students: [{
        id: {
            type: Number
        },
        name: {
            type: String
        }
    }],
    subGroup: [{
        type: Schema.Types.ObjectId,
        ref: 'Group'
    }],

    edu: {
        start: {
            type: String
        },
        end: {
            type: String
        },
        days: [{
            type: String,
            enum: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
        }],
        time: {
            type: String
        },
        credits: {
            type: Number
        }
    }

});

export default mongoose.model('Group', GroupSchema);