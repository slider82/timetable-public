import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
// import { Link } from 'react-router';
// import Promise from 'bluebird';

import signup from '../actions/SignupActions';

class Signup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: false,
            errorEmail: false,
            errorPassword: false,
            errorName: false,
            submitDisabled: true
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onInputFocus = this.onInputFocus.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { user, dispatch } = nextProps;

        if (user.error) {
            let error = {
                error: true
            };

            switch (user.error.message) {
                case 'INCORRECT_EMAIL':
                    error.errorEmail = true;
                    break;
                case 'INCORRECT_PASSWORD':
                    error.errorPassword = true;
                    break;
                case 'INCORRECT_NAME':
                    error.errorName = true;
                    break;
                case 'EMPTY_FIELDS':
                    error.errorEmail = true;
                    error.errorPassword = true;
                    error.errorName = true;
                    break;
            }

            this.setState(error);
        }

        if (user.isAuthorized) {
            dispatch(push('/'));
        }
    }

    getFormFields() {
        return {
            email: this.emailInput.value.trim(),
            password: this.passwordInput.value.trim(),
            name: this.nameInput.value.trim()
        }
    }

    validate() {
       return true;
    }

    onInputFocus() {
        if (this.state.error) {
            let error = {
                error: false
            };

            if (this.state.errorEmail) {
                error.errorEmail = false;
                this.emailInput.value = '';
            }

            if (this.state.errorPassword) {
                error.errorPassword = false;
                this.passwordInput.value = '';
            }

            if (this.state.errorName) {
                error.errorName = false;
                this.nameInput.value = '';
            }

            this.setState(error);
        }
    }

    onInputChange() {
        const formData = this.getFormFields();
        let submitDisabled;

        submitDisabled = !Object.keys(formData).every(key => formData[key]);

        if (this.state.submitDisabled !== submitDisabled) {
            this.setState({
                submitDisabled
            });
        }
    }

    onSubmit(event) {
        event.nativeEvent.preventDefault();

        if (this.validate()) {
            let data = this.getFormFields();

            this.props.signup(data);

            this.setState({
                error: false,
                errorEmail: false,
                errorPassword: false,
                errorName: false
            });
        } else {

        }
    }

    render() {
        const { user } = this.props;

        return(<div className="container">
            <div className="col-sm-6 col-sm-offset-3">
                <form onSubmit={this.onSubmit} className="form-horizontal">
                    <div className="col-xs-offset-2 col-xs-10">
                        <h1>Регистрация</h1>
                    </div>

                    {this.state.error ?
                        <div className="col-xs-offset-2 col-xs-10">
                            <div className="alert alert-danger" role="alert">Вы ввели не верные данные</div>
                        </div>
                    : null}
                    <div className={'form-group' + (this.state.errorEmail ? ' has-error' : '')}>
                        <label htmlFor="email" className="control-label col-xs-2">Email</label>
                        <div className="col-xs-10">
                            <input
                                className="form-control"
                                type="text"
                                name="email"
                                placeholder="Введите email"
                                ref={input => this.emailInput = input}
                                onChange={this.onInputChange}
                                onFocus={this.onInputFocus}/>
                        </div>
                    </div>
                    <div className={'form-group' + (this.state.errorPassword ? ' has-error' : '')}>
                        <label htmlFor="password" className="control-label col-xs-2">Пароль</label>
                        <div className="col-xs-10">
                            <input
                                className="form-control"
                                type="password"
                                name="password"
                                placeholder="Введите пароль"
                                ref={input => this.passwordInput = input}
                                onChange={this.onInputChange}
                                onFocus={this.onInputFocus}/>
                        </div>
                    </div>
                    <div className={'form-group' + (this.state.errorName ? ' has-error' : '')}>
                        <label htmlFor="name" className="control-label col-xs-2">Имя</label>
                        <div className="col-xs-10">
                            <input
                                className="form-control"
                                type="text"
                                name="name"
                                placeholder="Введите имя"
                                ref={input => this.nameInput = input}
                                onChange={this.onInputChange}
                                onFocus={this.onInputFocus}/>
                        </div>
                    </div>

                    <div className="form-group">
                        <div className="col-xs-offset-2 col-xs-10">
                            <button className="btn btn-primary" disabled={this.state.submitDisabled}>Регистрация</button>
                            {user.fetching ? 'Регистрация...' : ''}
                        </div>
                    </div>
                </form>
            </div>
        </div>);
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        signup: bindActionCreators(signup, dispatch),
        dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);