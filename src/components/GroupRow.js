import React, { Component } from 'react';

import StudentDateItem from './GroupStudentDateItem';

class GroupRow extends Component {
    render() {
        const {
            groupId,
            lang,
            dates,
            student,
            lessons,
            currentDate,
            onShowLessonForm
        } = this.props;
        const itemProps = {
            student,
            groupId,
            onShowLessonForm,
            currentDate
        };

        return(<tr>
            <td className="td-student">{student.shortName}</td>
            <td className="lang">{lang}</td>
            {dates.map(date => {
                const lesson = lessons[date];

                return <StudentDateItem
                    key={date}
                    {...lesson}
                    date={date}
                    {...itemProps}/>
            })}
        </tr>);
    }
}

export default GroupRow;