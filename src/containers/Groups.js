import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

import { getGroups } from '../actions/GroupsActions';

class Groups extends Component {
    constructor(props) {
        super(props);

        // this.onEditGroupClick = this.onEditGroupClick.bind(this);
        // this.onRemoveGroupClick = this.onRemoveGroupClick.bind(this);
    }

    componentDidMount() {
        if (!this.props.fetched) {
            this.props.getGroups();
        }
    }

    render() {
        const groups = this.props.groups.map(group => <Link key={group._id} to={'/group/' + group._id} className='list-group-item'>
            {group.name}
            {/*<span className="icons">*/}
                {/*<span className="glyphicon glyphicon-pencil" data-id={group._id} onClick={this.onEditGroupClick}></span>*/}
                {/*<span className="glyphicon glyphicon-trash" data-id={group._id} onClick={this.onRemoveGroupClick}></span>*/}
            {/*</span>*/}
        </Link>);


        return(
            <div className="container">
                <h2>Группы</h2>
                { this.props.fetching ? 'Загружаем' : null }
                { this.props.groups.length ?
                    <div className="col-sm-5" style={{
                        paddingLeft: 0
                    }}>
                        <div className="list-group groups">
                            { groups }
                        </div>
                    </div> : null
                }

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        groups: state.groups.items,
        fetching: state.groups.fetching,
        fetched: state.groups.fetched
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getGroups: bindActionCreators(getGroups, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Groups);