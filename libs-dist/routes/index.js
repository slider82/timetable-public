'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _auth = require('./auth');

var _auth2 = _interopRequireDefault(_auth);

var _api = require('./api');

var _api2 = _interopRequireDefault(_api);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    auth: _auth2.default,
    api: _api2.default
};