import React, { Component } from 'react';
import { connect } from 'react-redux'
import { push } from 'react-router-redux';


class Main extends Component {

    componentWillMount() {
        if (this.props.user.isAuthorized) {
            this.props.dispatch(push('/groups'));
        }
    }

    render() {
        return null;
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch: dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);