'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _config = require('../config');

var _User = require('../models/User');

var _User2 = _interopRequireDefault(_User);

var _group = require('../middlewares/group');

var _lesson = require('../middlewares/lesson');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = (0, _express.Router)();

// check auth
router.use(function (req, res, next) {
    var auth = req.headers.authorization;

    if (!auth) {
        res.status(401).json({
            error: 'No auth'
        });
    } else {
        var token = auth.split(' ')[1];

        if (token) {
            _jsonwebtoken2.default.verify(token, _config.jwtSecret, function (err, decoded) {
                if (err) {
                    res.status(401).json({
                        error: 'No auth'
                    });
                } else {
                    var userId = decoded.sub;

                    _User2.default.findById(userId, function (err, user) {
                        if (err) {
                            res.status(401).json({
                                error: 'User not found'
                            });
                        } else {
                            req.user = user;
                            next();
                        }
                    });
                }
            });
        }
    }
});

router.post('/group/create', _group.createGroup);
router.get('/group/get', _group.getGroups);

router.post('/lesson/save', _lesson.saveLesson);
router.post('/lesson/get', _lesson.getLessons);

exports.default = router;