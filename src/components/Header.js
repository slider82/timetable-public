import React, { Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import GroupCreateForm from './GroupCreateForm';
import { createGroup } from '../actions/GroupsActions';
import { logout } from '../actions/LoginActions';

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            showNavBar: false
        };

        this.onNavBarToggleClick = this.onNavBarToggleClick.bind(this);
        this.onCreateClick = this.onCreateClick.bind(this);
        this.onLogoutClick = this.onLogoutClick.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onClose = this.onClose.bind(this);

    }

    onNavBarToggleClick() {
        this.setState({
            showNavBar: !this.state.showNavBar
        });
    }

    onCreateClick(event) {
        this.setState({
            show: true
        });
    }

    onLogoutClick(event) {
        this.props.logout();
    }

    onSubmit(data) {
        this.props.createGroup(data);
    }

    onClose() {
        this.setState({
            show: false
        });
    }

    render() {
        let groupProps = {
                user: this.props.user,
                onSubmit: this.onSubmit,
                onClose: this.onClose,
                show: this.state.show
            };

        // из формы сделать общий компонент для создания, редактирования

        return (
            <header className="navbar navbar-default">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" onClick={this.onNavBarToggleClick}>
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <Link className={'navbar-brand'} to={'/'}>Register</Link>
                    </div>
                    <nav className={ 'navbar-collapse collapse collapsing' + (this.state.showNavBar ? ' in' : '')}>
                        {this.props.user.isAuthorized ?
                            <ul className="nav navbar-nav">
                                <li>
                                    <Link to="/groups">Группы</Link>
                                </li>
                            </ul>
                            : null }
                        {this.props.user.isAuthorized ?
                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                    <a onClick={this.onCreateClick}><span className="glyphicon glyphicon-plus"></span> Создать группу</a>
                                </li>
                                <li>
                                    <a onClick={this.onLogoutClick}><span className="glyphicon glyphicon-log-out"></span> Выйти</a>
                                </li>
                            </ul>
                        :
                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                    <Link to="/signup"><span className="glyphicon glyphicon-user"></span> Регистрация</Link>
                                </li>
                                <li>
                                    <Link to="/login"><span className="glyphicon glyphicon-log-in"></span> Войти</Link>
                                </li>
                            </ul>
                        }
                    </nav>
                </div>

                <GroupCreateForm {...groupProps} />

            </header>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    };
}
function mapDispatchToProps(dispatch) {
    return {
        createGroup: bindActionCreators(createGroup, dispatch),
        logout: bindActionCreators(logout, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);