import _ from 'lodash';
import moment from 'moment';
import User from '../models/User';
import Group from '../models/Group';

function validate(group) {
    let isValid = true;

    return {
        isValid
    };
}

function sortDate(a, b) {
    a = a.split('.');
    b = b.split('.');
    let a0 = +a[0];
    let b0 = +b[0];
    let a1 = +a[1];
    let b1 = +b[1];

    if (a1 > b1) {
        if (a0 > b0) {
            return 2;
        } else {
            return 1;
        }
    } else if (a1 < b1) {
        if (a0 > b0) {
            return -1;
        } else {
            return -2;
        }
    } else {
        if (a0 > b0) {
            return 0.5;
        } else {
            return -0.5;
        }
    }
}

function mapEdu(edu) {
    let formatDate = 'D.M',
        { start, end, days } = edu,
        startDay = moment(start, formatDate),
        endDay = moment(end, formatDate),
        today = moment(),
        result = [],
        currentDate,
        currentDates = [];

    days.forEach(item => {
        let beforeCurrent = startDay.clone().isoWeekday(item);

        for (
            let current = startDay.clone().isoWeekday(item);
            current.isSame(endDay) || current.isBefore(endDay);
            current.add(7, 'days')
        ) {
            // if (current.isSame(startDay) || current.isAfter(startDay)) {
            if (current.isSameOrAfter(startDay)) {
                let day = current.format('DD.MM');

                result.push(day);

                // вычисляем ближайщую дату до сегодняшней
                if (!currentDate) {
                    if (current.isSame(today, 'day')) {
                        currentDate = day;
                    } else {
                        if (current.isAfter(today) && beforeCurrent.isBefore(today)) {
                            currentDates.push(beforeCurrent.format('DD.MM'));
                        }
                    }

                    beforeCurrent = current.clone();
                }
            }
        }
    });

    if (!currentDate) {
        currentDate = Math.max.apply(Math, currentDates).toString();

        if (currentDates.length === 1) {
            currentDate = currentDates[0];
        } else {
            let firstDate = moment(currentDates[0], 'DD.MM');
            let secondDate = moment(currentDates[1], 'DD.MM');

            currentDate = firstDate.isAfter(secondDate) ? currentDates[0] : currentDates[1];
        }
    }

    edu.currentDate = currentDate;
    edu.dates = result.sort(sortDate);

    return edu;
}

function hashCode(str) {
    let hash = 0,
        char;

    if (str.length == 0) {
        return hash;
    }

    for (let i = 0; i < str.length; i++) {
        char = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash; // Convert to 32bit integer
    }

    return hash;
}

export function createGroup(req, res) {
    let group,
        options = {},
        body = req.body,
        user = body.teacher;

    // TODO: нужна проверка всех полей особенно дат!

    User.findById(user, (err, user) => {
        if (err)  {
            console.log(err);
            res.json({
                error: {
                    message: 'Can\'t find user'
                }
            });
        } else {
            let valid;

            _.assign(options, {
                name: body.name,
                teacher: user._id,
                lang: body.lang,
                type: body.type,
                students: body.students.map(student => {
                    return {
                        id: hashCode(student),
                        name: student
                    };
                }),
                edu: body.edu
            });


            // TODO: реализовать валидацию
            valid = validate(options);

            if (valid.isValid) {
                group = new Group(options);

                group.save((error, group) => {
                    if (error) {
                        console.log(error);

                        // TODO: обработать ошибки в клиенте
                        res.json({
                            error
                        });
                    } else {
                        group = group.toJSON();

                        _.merge(group, {
                            edu: mapEdu(group.edu)
                        });

                        res.json(group);
                    }

                });
            } else {
                // TODO: обработать ошибки в клиенте
                res.json({
                    error: valid.error
                });
            }

        }
    });
}

export function getGroups(req, res) {
    const user = req.user || {};

    if (user._id) {
        Group.find({
            teacher: user._id
        }, (error, groups) => {
            if (error) {
                res.json({
                    error
                });
            } else {
                res.json({
                    groups: groups.map(group => {
                        group = group.toJSON();
                        return _.merge(group, {
                            edu: mapEdu(group.edu)
                        })
                    })
                });
            }
        });
    } else {
        res.json({
            error: {
                message: 'Can\'t find user'
            }
        });
    }
}