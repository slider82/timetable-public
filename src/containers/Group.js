import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators} from 'redux';
import { push } from 'react-router-redux';

import { getGroups } from '../actions/GroupsActions';
import GroupTable from '../components/GroupTable';

class Group extends Component {

    componentDidMount() {
        // если нет групп то загружаем их
        if (!this.props.groups.length) {
            this.props.getGroups();
        }
    }

    componentWillReceiveProps(nextProps) {
        const group = this.getGroup(nextProps.groups);

        // если не нашли группу, видимо ее нет
        if (!group) {
            this.props.dispatch(push('/groups'));
        }
    }

    getGroup(groups) {
        const id = this.props.params.id;
        const group = groups.filter(item => item._id === id)[0];

        return group;
    }


    render() {
        const group = this.getGroup(this.props.groups);

        return(
            <div className="container">
                { group ? <GroupTable group={group} /> : 'Загружаем'}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        groups: state.groups.items
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getGroups: bindActionCreators(getGroups, dispatch),
        dispatch
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Group);
