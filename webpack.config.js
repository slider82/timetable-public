var path = require('path');
var webpack = require('webpack');
var LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

const production = process.env.NODE_ENV === 'production';

const entry = production ? [
        './src/css/style.scss',
        './src/index'
    ] : [
        './src/css/style.scss',
        'react-hot-loader/patch',
        'webpack-hot-middleware/client',
        './src/index'
    ];

const plugins = production ? [
        new LodashModuleReplacementPlugin({
            paths: false
        }),
        new webpack.DefinePlugin({
            isProduction: production
        }),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            comments: false,
            compress: {
                warnings: false,
                unused: true
            },
        }),
    ] : [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            'isProduction': production
        })
    ];

const devtool = production ? false : 'cheap-source-map';

module.exports = {
    devtool: devtool,
    entry: entry,
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/static/'
    },
    plugins: plugins,
    module: {
        preLoaders: [
            {
                test: /\.js$/,
                loaders: ['eslint-loader'],
                include: [
                    path.resolve(__dirname, 'src'),
                ],
                exclude: /node_modules/,
            }
        ],
        loaders: [
            {
                test: /\.js$/,
                loaders: ['react-hot-loader/webpack', 'babel'],
                include: path.join(__dirname, 'src')
            },
            {
                test: /\.scss$/,
                loaders: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=image/svg+xml'
            }
        ]
    }
};
