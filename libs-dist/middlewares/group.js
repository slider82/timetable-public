'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _merge2 = require('lodash/merge');

var _merge3 = _interopRequireDefault(_merge2);

var _assign2 = require('lodash/assign');

var _assign3 = _interopRequireDefault(_assign2);

exports.createGroup = createGroup;
exports.getGroups = getGroups;

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _User = require('../models/User');

var _User2 = _interopRequireDefault(_User);

var _Group = require('../models/Group');

var _Group2 = _interopRequireDefault(_Group);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function validate(group) {
    var isValid = true;

    return {
        isValid: isValid
    };
}

function sortDate(a, b) {
    a = a.split('.');
    b = b.split('.');
    var a0 = +a[0];
    var b0 = +b[0];
    var a1 = +a[1];
    var b1 = +b[1];

    if (a1 > b1) {
        if (a0 > b0) {
            return 2;
        } else {
            return 1;
        }
    } else if (a1 < b1) {
        if (a0 > b0) {
            return -1;
        } else {
            return -2;
        }
    } else {
        if (a0 > b0) {
            return 0.5;
        } else {
            return -0.5;
        }
    }
}

function mapEdu(edu) {
    var formatDate = 'D.M',
        start = edu.start,
        end = edu.end,
        days = edu.days,
        startDay = (0, _moment2.default)(start, formatDate),
        endDay = (0, _moment2.default)(end, formatDate),
        today = (0, _moment2.default)(),
        result = [],
        currentDate = void 0,
        currentDates = [];


    days.forEach(function (item) {
        var beforeCurrent = startDay.clone().isoWeekday(item);

        for (var current = startDay.clone().isoWeekday(item); current.isSame(endDay) || current.isBefore(endDay); current.add(7, 'days')) {
            // if (current.isSame(startDay) || current.isAfter(startDay)) {
            if (current.isSameOrAfter(startDay)) {
                var day = current.format('DD.MM');

                result.push(day);

                // вычисляем ближайщую дату до сегодняшней
                if (!currentDate) {
                    if (current.isSame(today, 'day')) {
                        currentDate = day;
                    } else {
                        if (current.isAfter(today) && beforeCurrent.isBefore(today)) {
                            currentDates.push(beforeCurrent.format('DD.MM'));
                        }
                    }

                    beforeCurrent = current.clone();
                }
            }
        }
    });

    if (!currentDate) {
        currentDate = Math.max.apply(Math, currentDates).toString();

        if (currentDates.length === 1) {
            currentDate = currentDates[0];
        } else {
            var firstDate = (0, _moment2.default)(currentDates[0], 'DD.MM');
            var secondDate = (0, _moment2.default)(currentDates[1], 'DD.MM');

            currentDate = firstDate.isAfter(secondDate) ? currentDates[0] : currentDates[1];
        }
    }

    edu.currentDate = currentDate;
    edu.dates = result.sort(sortDate);

    return edu;
}

function hashCode(str) {
    var hash = 0,
        char = void 0;

    if (str.length == 0) {
        return hash;
    }

    for (var i = 0; i < str.length; i++) {
        char = str.charCodeAt(i);
        hash = (hash << 5) - hash + char;
        hash = hash & hash; // Convert to 32bit integer
    }

    return hash;
}

function createGroup(req, res) {
    var group = void 0,
        options = {},
        body = req.body,
        user = body.teacher;

    // TODO: нужна проверка всех полей особенно дат!

    _User2.default.findById(user, function (err, user) {
        if (err) {
            console.log(err);
            res.json({
                error: {
                    message: 'Can\'t find user'
                }
            });
        } else {
            var valid = void 0;

            (0, _assign3.default)(options, {
                name: body.name,
                teacher: user._id,
                lang: body.lang,
                type: body.type,
                students: body.students.map(function (student) {
                    return {
                        id: hashCode(student),
                        name: student
                    };
                }),
                edu: body.edu
            });

            // TODO: реализовать валидацию
            valid = validate(options);

            if (valid.isValid) {
                group = new _Group2.default(options);

                group.save(function (error, group) {
                    if (error) {
                        console.log(error);

                        // TODO: обработать ошибки в клиенте
                        res.json({
                            error: error
                        });
                    } else {
                        group = group.toJSON();

                        (0, _merge3.default)(group, {
                            edu: mapEdu(group.edu)
                        });

                        res.json(group);
                    }
                });
            } else {
                // TODO: обработать ошибки в клиенте
                res.json({
                    error: valid.error
                });
            }
        }
    });
}

function getGroups(req, res) {
    var user = req.user || {};

    if (user._id) {
        _Group2.default.find({
            teacher: user._id
        }, function (error, groups) {
            if (error) {
                res.json({
                    error: error
                });
            } else {
                res.json({
                    groups: groups.map(function (group) {
                        group = group.toJSON();
                        return (0, _merge3.default)(group, {
                            edu: mapEdu(group.edu)
                        });
                    })
                });
            }
        });
    } else {
        res.json({
            error: {
                message: 'Can\'t find user'
            }
        });
    }
}