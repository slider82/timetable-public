import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

export function requireAuthentication(Component) {

    class Authenticated extends React.Component {

        componentWillMount() {
            this.checkAuth(this.props.isAuthorized);
        }

        componentWillReceiveProps(nextProps) {
            this.checkAuth(nextProps.isAuthorized);
        }

        checkAuth(isAuthorized) {
            if (!isAuthorized) {
                let redirectAfterLogin = this.props.location.pathname;
                this.props.dispatch(push(`/login?next=${redirectAfterLogin}`));
            }
        }

        render() {
            return (
                <div>
                    {this.props.isAuthorized === true
                        ? <Component {...this.props}/>
                        : null
                    }
                </div>
            )

        }
    }

    function mapStateToProps(state) {
        return {
            user: state.user,
            isAuthorized: state.user.isAuthorized
        }
    }

    return connect(mapStateToProps)(Authenticated);

}