import fetch from '../api/fetch';
import {
    SIGNUP_REQUEST,
    SIGNUP_SUCCESS,
    SIGNUP_FAIL
} from '../constaint/user';

export default function signup (data) {
    return function (dispatch) {
        dispatch({
            type: SIGNUP_REQUEST
        });

        fetch('/auth/signup', {
            method: 'POST',
            body: JSON.stringify(data)
        }).then(data => {
            if (data.error) {
                dispatch({
                    type: SIGNUP_FAIL,
                    error: data.error
                });
            } else {
                dispatch({
                    type: SIGNUP_SUCCESS,
                    ...data
                });
            }
        }).catch(error => {
            dispatch({
                type: SIGNUP_FAIL,
                error
            });
        });

    }
}